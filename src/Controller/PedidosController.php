<?php

namespace App\Controller;

use App\Entity\Pedidos;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PedidosController extends AbstractController
{
    #[Route('/pedidos', name: 'app_pedidos')]
    public function index(EntityManagerInterface $entityManager): Response
    {
        // Obtém o repositório da entidade Pedidos
        $repository = $entityManager->getRepository(Pedidos::class);

        // Consulta todos os pedidos
        $pedidos = $repository->findAll();

        // Renderiza o template passando os pedidos como parâmetro
        return $this->render('pedidos/index.html.twig', [
            'pedidos' => $pedidos,
        ]);
    }
}
